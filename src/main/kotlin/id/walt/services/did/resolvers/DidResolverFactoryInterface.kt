package id.walt.services.did.resolvers

interface DidResolverFactoryInterface {
    fun create(didMethod: String): DidResolver
}
